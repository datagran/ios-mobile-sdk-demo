//
//  CustomTrackingViewController.swift
//  DatagranTrackerExampleApp
//
//  Created by David A Cespedes R on 6/20/19.
//  Copyright © 2019 David A Cespedes R. All rights reserved.
//

import UIKit
import Tracker_iOS

class CustomTrackingViewController: UIViewController {

  @IBOutlet weak var trackingSwitch: UISwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    Tracker.shared.trackViews(freindlyN: "CustomTracking View",
                              ObjRef: self.view!,
                              ViewRef: self.view!,
                              controller: self,
                              addGeo: true)
    
  }
  
  @IBAction func switchValueChanged(_ sender: UISwitch) {
    print("Switch is ON: \(sender.isOn)")
    Tracker.shared.trackCustom(category: "Switch",
                               action: "ValueChanged",
                               lable: sender.isOn ? "ON" : "OFF",
                               controller: self,
                               addGeo: sender.isOn)
  }
  
}

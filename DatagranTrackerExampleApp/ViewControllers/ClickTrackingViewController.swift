//
//  ClickTrackingViewController.swift
//  DatagranTrackerExampleApp
//
//  Created by David A Cespedes R on 6/17/19.
//  Copyright © 2019 David A Cespedes R. All rights reserved.
//

import UIKit
import Tracker_iOS

class ClickTrackingViewController: UIViewController {

  @IBOutlet weak var clickButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    Tracker.shared.trackViews(freindlyN: "ClickTracking View",
                              ObjRef: self.view!,
                              ViewRef: self.view!,
                              controller: self,
                              addGeo: true)
  }
  
  @IBAction func buttonClicked(_ sender: UIButton) {
    print("Button Clicked")
    Tracker.shared.track(sender: sender, eventAction: .onClick, controller: self, addGeo: false)
  }
  


}


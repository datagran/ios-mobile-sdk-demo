//
//  TextFieldTrackingViewController.swift
//  DatagranTrackerExampleApp
//
//  Created by David A Cespedes R on 6/20/19.
//  Copyright © 2019 David A Cespedes R. All rights reserved.
//

import UIKit
import Tracker_iOS

class TextFieldTrackingViewController: UIViewController {

  @IBOutlet weak var trackingTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    Tracker.shared.trackViews(freindlyN: "TextFieldTracking View",
                              ObjRef: self.view!,
                              ViewRef: self.view!,
                              controller: self,
                              addGeo: true)
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
    view.addGestureRecognizer(tapGesture)
  }
  
  @IBAction func trackingTextFieldEditingBegin(_ sender: UITextField) {
    print("Textfield Begin")
    Tracker.shared.track(sender: sender, eventAction: .editingDidBegin, controller: self, addGeo: true)
  }
  
  @IBAction func trackingTextFieldEditingChange(_ sender: UITextField) {
    print("Textfield Change")
    Tracker.shared.track(sender: sender, eventAction: .editingChanged, controller: self, addGeo: false)
  }
  
  @IBAction func trackingTextFieldEditingEnd(_ sender: UITextField) {
    print("Textfield End")
    Tracker.shared.track(sender: sender, eventAction: .editingDidEnd, controller: self, addGeo: true)
    if sender == trackingTextField {
      Tracker.shared.identify(email: trackingTextField.text ?? "No String in text field", controller: self, addGeo: true)
    }
  }
  
  @objc func hideKeyboard() {
    view.endEditing(true)
  }
}

extension TextFieldTrackingViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == trackingTextField {
      textField.resignFirstResponder()
    }
    return true
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if textField == trackingTextField {
      print("Textfield Change with: \(string)")
      Tracker.shared.track(sender: textField, eventAction: .editingChanged, controller: self, characterKey: string, addGeo: true)
    }
    return true
  }
}

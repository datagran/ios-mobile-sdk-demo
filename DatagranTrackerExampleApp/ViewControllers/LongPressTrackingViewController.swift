//
//  LongPressTrackingViewController.swift
//  DatagranTrackerExampleApp
//
//  Created by David A Cespedes R on 6/17/19.
//  Copyright © 2019 David A Cespedes R. All rights reserved.
//

import UIKit
import Tracker_iOS

class LongPressTrackingViewController: UIViewController {

  @IBOutlet weak var longPressButton: UIButton!
  
  var longPressGesture: UILongPressGestureRecognizer?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    Tracker.shared.trackViews(freindlyN: "LongPressTracking View",
                              ObjRef: self.view!,
                              ViewRef: self.view!,
                              controller: self,
                              addGeo: true)
    
    longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(buttonLongPressed(_:)))
    longPressButton.addGestureRecognizer(longPressGesture!)
  }

  
  @objc func buttonLongPressed(_ sender : UIButton) {
    if let longPressGesture = self.longPressGesture,
      longPressGesture.state == .ended {
      print("Button Longpressed")
      Tracker.shared.track(sender: sender, eventAction: .onLongPress, controller: self, addGeo: true)
    }
    
  }

}

